package sample

import pigpio.*

fun main(args: Array<String>) {
    val PORT = 21u

    if (gpioInitialise() < 0) {
        println("Error Initializing")
    } else {
        if (gpioSetMode(PORT, PI_OUTPUT) < 0) {
            println("Could not set mode for GPIO$PORT")
        } else {
            var level = 1u
            while(true) {
                if (gpioWrite(PORT, level) < 0) {
                    println("Could not write to GPIO$PORT")
                }
                level = (level + 1u) % 2u
                gpioSleep(PI_TIME_RELATIVE, 0, 500000)
            }
        }
    }
}